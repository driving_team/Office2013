@ECHO OFF
title ★ office2013 四合一绿色精简版 ★
mode con: cols=80 lines=22
ver | find "5.0." > NUL && set winos=xpos
ver | find "5.1." > NUL && set winos=xpos
ver | find "5.2." > NUL && set winos=xpos
ver | find "6.0." > NUL && set winos=win7os
ver | find "6.1." > NUL && set winos=win7os
ver | find "6.2." > NUL && set winos=win8os
ver | find "6.3." > NUL && set winos=win8os
ver | find "6.4." > NUL && set winos=win8os
ver | find "10." > NUL && set winos=win8os
IF /i %winos%==xpos (echo     Office2013 不支持该系统！&& pause>nul && exit)
PUSHD %~dp0
SET PATH=%PATH%;%CD%;%CD%\ospp
IF /i %PROCESSOR_ARCHITECTURE%==AMD64 (
    SET ARCH=X64
    SET SYSPath=%SystemRoot%\sysWOW64
    SET "MSharedPATH=%CommonProgramFiles(x86)%\Microsoft Shared"
) ELSE (
    SET ARCH=X86
    SET SYSPath=%SystemRoot%\system32
    SET "MSharedPATH=%CommonProgramFiles%\Microsoft Shared"
)
color 2f
cls
echo.
echo.
echo                      Office2013 pro plus vol 四合一绿色精简版
echo.
echo   ---------------------------- 2016.04.22 by xb21cn --------------------------
echo.
echo                      支持 win7 win8 win8.1 win10 x86/x64 系统
echo.
echo.
echo  正在安装，请耐心等待…………
if NOT EXIST "%MSharedPATH%\VC\msdia100.dll" (START /wait ospp\vc2010.exe /q)
"%~dp0ospp\7za.exe" e -y "ospp\QuickToobar.7z" -o"%UserProfile%\AppData\Local\Microsoft\Office">nul 2>nul
"%~dp0ospp\7za.exe" x -y "ospp\MShared.7z" -o"%MSharedPATH%">nul 2>nul
"%~dp0ospp\7za.exe" x -y "ospp\ShellNew.7z" -o"%Windir%" >nul 2>nul
REGSVR32 /s "%MSharedPATH%\MSClientDataMgr\MSCDM.dll"
REGSVR32 /s "%MSharedPATH%\Office15\MSOXEV.dll"
REGSVR32 /s "%MSharedPATH%\Office15\msoshext.dll"
REGSVR32 /s "%MSharedPATH%\VBA\VBA7.1\VBE7.dll"
REGSVR32 /s "Office15\Addins\OTKLOADR.dll"
REGSVR32 /s "Office15\ACCWIZ.dll"
REGSVR32 /s "Office15\IEAWSDC.dll"
REGSVR32 /s "Office15\MSOEURO.dll"
REGSVR32 /s "Office15\MSOHEV.dll"
REGSVR32 /s "Office15\MSOHEVI.dll"
REGSVR32 /s "Office15\MSRTEDIT.dll"
REGSVR32 /s "Office15\REFEDIT.dll"
REGSVR32 /s "Office15\NAME.dll"
REGSVR32 /s "Office15\OSF.dll"
REGSVR32 /s "Office15\MSADDNDR.dll"
COPY /y "Office15\FM20*.DLL" "%SYSPath%">nul 2>nul
COPY /y "Office15\VEN2232.OLB" "%SYSPath%">nul 2>nul
REGSVR32 /s "%SYSPath%\FM20.DLL"
set route=%cd%
set route=%route:\=\\%
copy "Office15\office.dat" "%temp%\test.dat" /y>nul 2>nul
"%~dp0ospp\FR.exe" "%temp%\test.dat" -fic:"C:\\" -t:"%SystemDrive%\\"
"%~dp0ospp\FR.exe" "%temp%\test.dat" -fic:"D:\\office2013" -t:"%route%"
IF /i %ARCH%==X86 (
   "%~dp0ospp\FR.exe" "%temp%\test.dat" -fic:"SysWOW64" -t:"system32"
   "%~dp0ospp\FR.exe" "%temp%\test.dat" -fic:"Program Files (x86)" -t:"Program Files"
   "%~dp0ospp\FR.exe" "%temp%\test.dat" -fic:"\Wow6432Node" -t:""
)
regedit /s "%temp%\test.dat"
del /q "%temp%\test.dat"
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\Word 2013.lnk""):b.TargetPath=""%CD%\office15\WINWORD.exe"":b.WorkingDirectory=""%~dp0"":b.Save:close")&SET E=完成!
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\Excel 2013.lnk""):b.TargetPath=""%CD%\office15\EXCEL.exe"":b.WorkingDirectory=""%~dp0"":b.Save:close")&SET E=完成!
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\PowerPnt2013.lnk""):b.TargetPath=""%CD%\office15\POWERPNT.EXE"":b.WorkingDirectory=""%~dp0"":b.Save:close")&SET E=完成!
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\Access2013.lnk""):b.TargetPath=""%CD%\office15\MSACCESS.EXE"":b.WorkingDirectory=""%~dp0"":b.Save:close")&SET E=完成!
reg add "HKCU\Software\Microsoft\Office\15.0\Registration\%COMPUTERNAME%\{90150000-0011-0000-0000-0000000FF1CE}\EULA" /f /v "8" /t REG_SZ /d "">nul 2>nul
if NOT EXIST "%windir%\fonts\cambria.ttf" "%~dp0ospp\fontinst.exe"
if NOT EXIST "%APPDATA%\Microsoft\Office" md "%APPDATA%\Microsoft\Office">nul 2>nul
copy "Office15\MSO0127.acl" "%APPDATA%\Microsoft\Office\MSO0127.acl" /y >nul 2>nul
if not EXIST "%APPDATA%\Microsoft\Document Building Blocks\2052\15" md "%APPDATA%\Microsoft\Document Building Blocks\2052\15">nul 2>nul
copy "Office15\page.dotx" "%APPDATA%\Microsoft\Document Building Blocks\2052\15\Built-In Building Blocks.dotx" /y >nul 2>nul
reg add "HKCU\Software\Microsoft\Office\15.0\PowerPoint\RecentFolderList" /f /v "SoundDir" /t REG_EXPAND_SZ /d "%cd%\Office15\MEDIA\\"
regedit /s "Office15\officeu.dat"
regedit /s "ospp\set.dat"
echo [Version]>%temp%\re.inf
echo Signature="$CHICAGO$">>%temp%\re.inf
echo [Defaultinstall]>>%temp%\re.inf
RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultInstall 128 %temp%\re.inf
del /q %temp%\re.inf
IF /i %winos%==win7os goto OSPP
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\pkeyconfig-office.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\client-issuance-ul.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\client-issuance-ul-oob.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\client-issuance-bridge-office.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\ProPlus\ProPlusVL_KMS_Client-ppd.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\ProPlus\ProPlusVL_KMS_Client-ul.xrm-ms"
CSCRIPT.exe %Windir%\System32\slmgr.vbs /ilc "%CD%\key\ProPlus\ProPlusVL_KMS_Client-ul-oob.xrm-ms"
CSCRIPT.exe "%CD%\Office15\OSPP.vbs" /inpkey:YC7DK-G2NP3-2QQC3-J6H88-GVGXT
exit

:OSPP
copy "ospp\ospp.dat" "%temp%\ospp.reg" /y >nul 2>nul
set OSPPreg=%ProgramFiles:\=\\%
"%~dp0ospp\FR.exe" "%temp%\ospp.reg" -fic:"C:\\Program Files" -t:"%OSPPreg%"
regedit /s "%temp%\ospp.reg"
DEL /f /q "%temp%\ospp.reg"
IF /i %ARCH%==X64 (
    "%~dp0ospp\7za.exe" x -y "ospp\OSPP_X64.7z" -o"%CommonProgramFiles%\Microsoft Shared" >nul 2>nul
    "%~dp0ospp\7za.exe" x -y "ospp\OSPP_X64_X86.7z" -o"%CommonProgramFiles(x86)%\Microsoft Shared" >nul 2>nul
) ELSE (
    "%~dp0ospp\7za.exe" x -y "ospp\OSPP_X86.7z" -o"%CommonProgramFiles%\Microsoft Shared" >nul 2>nul
)
net localgroup administrators "Network Service" /add >nul 2>nul
set "ospppath=%CommonProgramFiles%\Microsoft Shared\OfficeSoftwareProtectionPlatform"
set "osppdata=%AllUsersProfile%\Microsoft\OfficeSoftwareProtectionPlatform"
Mofcomp.exe "%ospppath%\OSPPWMI.MOF" >nul 2>nul
REGSVR32 /s "%ospppath%\OSPPWMI.dll"
IF NOT EXIST "%osppdata%" md "%osppdata%"
copy "ospp\tokens7.dat" "%osppdata%\tokens.dat" /y
SC.exe create osppsvc start= DEMAND obj= "NT AUTHORITY\NetworkService" Binpath= "%ospppath%\OSPPSVC.exe" DisplayName= "Office Software Protection Platform" Depend= RpcSs >nul 2>nul
SC.exe Description osppsvc "微软office正版验证程序"
SC.exe sdset osppsvc "D:(A;;CCLCSWRPLO;;;NS)(A;;CCLCSWRPLO;;;IU)(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;SU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
"%~dp0office15\OSPPREARM.EXE"
exit
