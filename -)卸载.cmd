@echo off
PUSHD %~dp0
SET PATH=%PATH%;%CD%;%CD%\ospp
IF /i %PROCESSOR_ARCHITECTURE%==AMD64 (
    SET ARCH=X64
    SET SYSPath=%SystemRoot%\sysWOW64
    SET "MSharedPATH=%CommonProgramFiles(x86)%\Microsoft Shared"
) ELSE (
    SET ARCH=X86
    SET SYSPath=%SystemRoot%\system32
    SET "MSharedPATH=%CommonProgramFiles%\Microsoft Shared"
)
cls
regedit /s "%~dp0ospp\unist.dat"
copy "%~dp0ospp\unist.dat" "%temp%\unist.reg" /y>nul 2>nul
"%~dp0ospp\FR.exe" "%temp%\unist.reg" -fic:"\Wow6432Node" -t:""
regedit /s "%temp%\unist.reg"
del /q "%temp%\unist.reg"
REGSVR32 /u /s "%MSharedPATH%\MSClientDataMgr\MSCDM.dll"
REGSVR32 /u /s "%MSharedPATH%\Office15\MSOXEV.dll"
REGSVR32 /u /s "%MSharedPATH%\Office15\msoshext.dll"
REGSVR32 /u /s "%MSharedPATH%\VBA\VBA7.1\VBE7.dll"
REGSVR32 /u /s "Office15\Addins\OTKLOADR.dll"
REGSVR32 /u /s "Office15\ACCWIZ.dll"
REGSVR32 /u /s "Office15\IEAWSDC.dll"
REGSVR32 /u /s "Office15\MSOEURO.dll"
REGSVR32 /u /s "Office15\MSOHEV.dll"
REGSVR32 /u /s "Office15\MSOHEVI.dll"
REGSVR32 /u /s "Office15\MSRTEDIT.dll"
REGSVR32 /u /s "Office15\REFEDIT.dll"
REGSVR32 /u /s "Office15\NAME.dll"
REGSVR32 /u /s "Office15\OSF.dll"
REGSVR32 /u /s "Office15\MSADDNDR.dll"
REGSVR32 /u /s "%SYSPath%\FM20.DLL"
del "%SYSPath%\fm20.dll"
del "%SYSPath%\fm20chs.dll"
del "%SYSPath%\VEN2232.OLB"
rd /s /q "%MSharedPATH%\Equation"
rd /s /q "%MSharedPATH%\MSClientDataMgr"
rd /s /q "%MSharedPATH%\Office15"
rd /s /q "%MSharedPATH%\VBA"
rd /s /q "%MSharedPATH%\Filters"
rd /s /q "%MSharedPATH%\Portal"
del "%windir%\SHELLNEW\ACCESS.MDB"
del "%windir%\SHELLNEW\EXCEL.XLS"
del "%windir%\SHELLNEW\EXCEL12.XLSX"
del "%windir%\SHELLNEW\PWRPNT.PPT"
rd /s /q "%APPDATA%\Microsoft\Access"
rd /s /q "%APPDATA%\Microsoft\PowerPoint"
rd /s /q "%APPDATA%\Microsoft\Office"
rd /s /q "%APPDATA%\Microsoft\Excel"
rd /s /q "%APPDATA%\Microsoft\Word"
rd /s /q "%APPDATA%\Microsoft\Templates"
rd /s /q "%APPDATA%\Microsoft\AddIns"
rd /s /q "%APPDATA%\Microsoft\Proof"
rd /s /q "%UserProfile%\AppData\Local\Microsoft\Office"
rd /s /q "%appdata%\Microsoft\Document Building Blocks"
rd /s /q "%appdata%\Microsoft\CLView"
rd /s /q "%appdata%\Microsoft\UProof"
rd /s /q "%appdata%\Microsoft\Bibliography"
rd /s /q "%appdata%\Microsoft\QuickStyles"
rd /s /q "%appdata%\Microsoft\Spelling"
del "%USERPROFILE%\Desktop\Word 2013.lnk"
del "%USERPROFILE%\Desktop\excel 2013.lnk"
del "%USERPROFILE%\Desktop\PowerPnt2013.lnk"
del "%USERPROFILE%\Desktop\Access2013.lnk"
For /f "delims=*" %%i in ('Reg Query "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "Desktop" ') do Set a=%%i
set ppath=%a:~32,256%
del "%ppath%\Word 2013.lnk"
del "%ppath%\excel 2013.lnk"
del "%ppath%\PowerPnt2013.lnk"
del "%ppath%\Access2013.lnk"
net stop osppsvc >nul 2>nul
sc delete osppsvc >nul 2>nul
net localgroup administrators "Network Service" /del >nul 2>nul
regsvr32 /u /s "%MSharedPATH%\OfficeSoftwareProtectionPlatform\OSPPWMI.dll"
takeown /f "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\tokens.dat"
icacls "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\tokens.dat" /grant administrators:F /t
rd /s /q "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform"
rd /s /q "%CommonProgramFiles%\Microsoft Shared\OfficeSoftwareProtectionPlatform"
rd /s /q "%CommonProgramFiles(x86)%\Microsoft Shared\OfficeSoftwareProtectionPlatform"
echo [Version]>%temp%\re.inf
echo Signature="$CHICAGO$">>%temp%\re.inf
echo [Defaultinstall]>>%temp%\re.inf
RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultInstall 128 %temp%\re.inf
del /q %temp%\re.inf
exit
