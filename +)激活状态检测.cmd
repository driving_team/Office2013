@ECHO off
FOR /f "tokens=2 delims=[]" %%i in ('VER') DO SET NTv=%%i
FOR /f "tokens=2,3 delims=. " %%i in ("%NTv%") DO (
    SET NTv=%%i.%%j
    SET NTmv=%%i
)

IF %NTv% EQU 6.1 (REG.exe query "HKLM\SYSTEM\ControlSet001\Services\OSPPSvc">nul 2>nul || (ECHO. & ECHO 系统缺少OFFICE必须的OSPPSvc服务，按任意键退出... & PAUSE>nul & Exit))
IF %NTv% EQU 6.0 (REG.exe query "HKLM\SYSTEM\ControlSet001\Services\OSPPSvc">nul 2>nul || (ECHO. & ECHO 系统缺少OFFICE必须的OSPPSvc服务，按任意键退出... & PAUSE>nul & Exit))

CLS
ECHO.
ECHO 正在获取激活信息...
SET "#=%temp%\#.txt"
IF EXIST %#% DEL /f /q %#%
CSCRIPT.exe //nologo office15\ospp.vbs /dstatus | FIND.exe /i ":">>%#%
FOR /F "eol=- tokens=2 delims=:" %%i IN ('FIND.exe /i "SKU ID" %#%') do (SET "SKU ID=%%i")
FOR /F "eol=- tokens=2 delims=:" %%i IN ('FIND.exe /i "LICENSE NAME" %#%') do (SET "LICENSE NAME=%%i")
FOR /F "eol=- tokens=2 delims=:" %%i IN ('FIND.exe /i "LICENSE DESCRIPTION" %#%') do (SET "LICENSE DESCRIPTION=%%i")
FOR /F "eol=- tokens=2 delims=:" %%i IN ('FIND.exe /i "LICENSE STATUS" %#%') do (SET "LICENSE STATUS=%%i")
FOR /F "eol=- tokens=2 delims=:" %%i IN ('FIND.exe /i "REMAINING GRACE" %#%') do (SET "REMAINING GRACE=%%i")
IF EXIST %#% DEL /f /q %#%

IF /i %PROCESSOR_ARCHITECTURE%==AMD64 (
    REG.exe query HKLM\SOFTWARE\Wow6432Node\Microsoft\Office\15.0\Common\InstallRoot /v Path>nul 2>nul && SET Bit=32Bit 
    REG.exe query HKLM\SOFTWARE\Microsoft\Office\15.0\Common\InstallRoot /v Path>nul 2>nul && SET Bit=64Bit 
) ELSE (
    REG.exe query HKLM\SOFTWARE\Microsoft\Office\15.0\Common\InstallRoot /v Path>nul 2>nul && SET Bit=32Bit 
)

IF /i "%LICENSE STATUS%" EQU "  ---LICENSED--- " (
    IF "%REMAINING GRACE%" NEQ "" (
        COLOR 1F & SET MSG=OFFICE 处于 KMS 激活 状态！
    ) ELSE (
        COLOR 2F & SET MSG=OFFICE 处于 永久激活 状态！
    )
) ELSE IF /i "%LICENSE STATUS%" EQU "  ---OOT_GRACE--- " (
        COLOR 3F & SET MSG=OFFICE 处于试用状态，请使用kms等工具进行激活。
) ELSE (
    COLOR 4F & SET MSG=OFFICE 未获得授权，请使用kms等工具进行激活。
)

CLS
ECHO.
ECHO *******************************************************************************
ECHO  ***                       OFFICE 2013 %Bit% 激活状态                      ***
ECHO *******************************************************************************
echo.
ECHO     商品编号: %SKU ID%
echo.
ECHO     许可名称: %LICENSE NAME%
echo.
ECHO     许可类型: %LICENSE DESCRIPTION%
echo.
IF NOT "%REMAINING GRACE%"=="" ECHO     剩余时间: %REMAINING GRACE%
echo.
ECHO     许可状态: %LICENSE STATUS%
echo.
ECHO *******************************************************************************
ECHO.
echo.
ECHO   　　        ★★★　%MSG%　★★★ 
ECHO.
PAUSE>nul
Exit
