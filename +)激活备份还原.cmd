@echo off
title ＝＝＝ 手动备份恢复office 2013 pro sp1 vol 激活信息工具＝＝＝
echo.
echo.
echo 恢复前，请确认密钥为原始安装激活成功的密钥，并且，硬件没有改动！
echo.
echo 如果不能确认请直接关闭本程序！
pause>nul
echo.
ver | find "6.0." > NUL && goto win7
ver | find "6.1." > NUL && goto win7
cls
color 2F
echo.
echo.                  正版激活信息备份恢复
echo. ………………………………………………………………………………………
echo.
echo.           1.  一键备份 Windows8/10 Office2013激活信息
echo.    
echo.           2.  一键恢复 Windows8/10 Office2013激活信息
echo.    
echo.           3.  显示系统是否彻底激活
echo.    
echo.           4.  退出
echo. ………………………………………………………………………………………
echo.
set osppkey=%WINDIR%\System32\spp\store\2.0
ver | find "6.2." > NUL && set osppkey=%WINDIR%\System32\spp\store
ver | find "10." > NUL && IF EXIST "%WINDIR%\System32\spp\store_test" (
    set "osppkey=%WINDIR%\System32\spp\store_test\2.0"
) else (
    set "osppkey=%WINDIR%\System32\spp\store\2.0"
)
set /p c=请选择:
if %c%==1 goto 1
if %c%==2 goto 2
if %c%==3 goto 3
if %c%==4 exit
:1
If NOT Exist %~dp0store (MD %~dp0store)
If NOT Exist %~dp0store\cache  (MD %~dp0store\cache)
attrib -h "%osppkey%\data.dat"
copy "%osppkey%\data.dat" %~dp0store\data.dat /-y
copy "%osppkey%\cache\cache.dat" %~dp0store\cache\cache.dat /-y
copy "%osppkey%\tokens.dat" %~dp0store\tokens.dat /-y
attrib +h "%osppkey%\data.dat"
echo.
echo.
set /p x=备份Windows8激活密钥（回车确定）：
set /p y=备份Office2013激活密钥（回车确定）：
echo %x%>%~dp0store\Win-key.txt
echo %y%>%~dp0store\Office-key.txt
color 2F
echo.
echo.
echo 激活文件成功备份到本软件同目录store文件夹中。复制到其它地方保存
echo.
pause >nul
:2
echo.
echo   请确认之前备份的store文件夹已放到本软件同目录中,
echo.
echo.
net stop sppsvc
takeown /f "%osppkey%" 
icacls "%osppkey%" /grant administrators:F /t
attrib -h "%~dp0store\data.dat"
attrib -h "%osppkey%\data.dat"
copy "%~dp0store\data.dat" "%osppkey%\" /y 
copy "%~dp0store\tokens.dat" "%osppkey%\" /y 
copy "%~dp0store\cache\cache.dat" "%osppkey%\cache\" /y 
attrib +h "%osppkey%\data.dat"
for /f "delims=," %%i in (%~dp0store\Office-key.txt) do cscript Office15\OSPP.VBS /inpkey:%%i
echo.
echo                    激活信息恢复完成！
echo.
slmgr -xpr
echo.
echo.
color 2F
pause >nul
exit
:3
slmgr.vbs -xpr
exit

:win7
cls
color 2F
echo.
echo.                  　　正版激活信息备份恢复
echo. ………………………………………………………………………………………
echo.
echo.
set "xxx="&set /p xxx=请选择（1.激活信息备份　2.激活信息还原）：
if "%xxx%" neq "1" if "%xxx%" neq "2" cls&goto begin
cls&goto xx%xxx%

:xx1
if NOT EXIST "bakkey" md "bakkey">nul 2>nul
copy /y "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\tokens.dat" "%~dp0bakkey\tokens.dat"
copy /y "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\Cache\cache.dat" "%~dp0bakkey\cache.dat"
echo.
echo.
set /p y=请输入备份 Office2013 激活密钥（×××-×××格式，按回车确定）：
echo %y%>bakkey\key.txt
echo.
echo.
echo          备份完成！按任意键退出 ......
pause >nul
exit

:xx2
net stop "osppsvc"
copy /y "%~dp0bakkey\tokens.dat" "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\"
copy /y "%~dp0bakkey\cache.dat" "%ALLUSERSPROFILE%\Microsoft\OfficeSoftwareProtectionPlatform\Cache\"
for /f "delims=," %%i in (%~dp0bakkey\key.txt) do cscript Office15\OSPP.VBS /inpkey:%%i
echo.
echo.
echo          恢复成功！按任意键退出 ......
pause >nul